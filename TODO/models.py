from django.db import models


class Todo(models.Model):
    name = models.CharField(max_length=120, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    deadline = models.DateTimeField(auto_now_add=True)
    interval = models.IntegerField(default=1)
