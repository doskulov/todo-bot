import telepot
import time
from observable import Observable

from Bot.apps import BotConfig
from Bot.decorators import check_status


class Bot:
    token = BotConfig.token
    url = BotConfig.url
    listener = Observable()
    bot = telepot.Bot(token)

    @staticmethod
    def do_commands(command, user):
        command = command.split()
        if command[0] == '/start':
            src = '.'+str(user.contract)
            Bot.sends(user.telegram.telegram_id, BotConfig.types[1], "./templates/contracts/123312123123.pdf")
            Bot.sends(user.telegram.telegram_id, BotConfig.types[0], BotConfig.text['in_start'])

    """
        отправляем юзеру сообщения
        user - телеграм id юзера
            Examples:
                message:
                    Bot.sends(123, 'message', 'Hello world')
                document:
                    Bot.sends(123, 'document', './static/english.docx')ц
    """
    @staticmethod
    def sends(user=None, type=None, *args):
        if type == 'document':
            doc = open(args[0], 'rb')
            Bot.bot.sendDocument(user, doc)

        elif type == 'message':
            bots = Bot.bot.sendMessage(user, args[0], parse_mode='Markdown')
            return bots

    @check_status(0)
    @check_status(1)
    def handle(self, message):
        automate_event(message)

    def __init__(self):
        self.bot.message_loop(self.handle)
        while 1:
            time.sleep(50)


events = {
    0: 'Bot.Reply',
    1: 'Bot.Photo',
    2: 'Bot.Document',
    3: 'Bot.Sticker',
    4: 'Bot.Video',
    5: 'Bot.Voice',
    6: 'Bot.Audio',
    7: 'Bot.Command'
}

conditions = [
    lambda x: x.__contains__('reply_to_message'),
    lambda x: x.__contains__('photo'),
    lambda x: x.__contains__('document'),
    lambda x: x.__contains__('sticker'),
    lambda x: x.__contains__('video'),
    lambda x: x.__contains__('voice'),
    lambda x: x.__contains__('audio'),
    lambda x: x.__contains__('entities'),
]


def automate_event(msg):
    for item in enumerate(conditions):
        index = item[0]
        condition = item[1]

        print(events[index]) if condition(msg) else None
        Bot.listener.trigger(events[index], msg) if condition(msg) else None