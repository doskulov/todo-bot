import os

from Bot.apps import BotConfig
from Bot.utils import validate_zip
from Bot.views import Bot


@Bot.listener.on("Bot.Message")
def message_(msg):
    print(msg['text'])


@Bot.listener.on("Bot.Document")
def document_(msg):
    from Auth.models import Person
    document = msg['document']
    user = msg['from']
    owner = Person.objects.get(telegram_id=user['id'])

    if owner.status == 1:
        src_name = './media/tech_docs/' + 'waiting_' + str(owner.iin) + '.zip'
        Bot.bot.download_file(document['file_id'], src_name)
        zip_errors = validate_zip(owner)

        if zip_errors.__len__() == 0:
            owner.status = 2
            owner.save()
            Bot.sends(user['id'], 'message', BotConfig.text['success_zip'])
        else:
            os.remove(src_name)
            error_msg = ""

            for error in zip_errors:
                error_msg += "- " + BotConfig.text[error] + "\n"

            Bot.sends(user['id'], 'message', error_msg)
    else:
        pass


@Bot.listener.on("Bot.Command")
def command_(msg):
    from Auth.models import Person
    command = msg['text']
    user = msg['from']
    owner = Person.objects.get(telegram_id=user['id'])
    Bot.do_commands(command, owner)


@Bot.listener.on("Bot.Reply")
def bot_reply(msg):
    Bot.sends(msg['from']['id'], 'message', 'hello')