import zipfile

def validate_zip(user):
    iin = str(user.iin)
    src = './media/tech_docs/waiting_' + str(user.iin) + '.zip'
    zip_ref = zipfile.ZipFile(src)
    list = zip_ref.namelist()
    zip_ref.close()

    error = set([])
    if list.__len__() != 4:
        error.add('zip_error_file_count')

    for i in list:
        content = i.split('.')
        if content.__len__() == 2:
            if content[1] != 'pdf':
                error.add('zip_error_type')
            text = content[0].split('_')
            if text.__len__() != 2:
                error.add('zip_error_template')
            if text[0] != iin:
                error.add('zip_error_iin')
        else:
            error.add('zip_error_name')
            break
    print(error)
    return error