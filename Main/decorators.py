def check_status(status):
    def check_status_decorator(func):
        def func_wrapper(*args):

            message = args[1]
            owner = message.get('from')
                return func(*args)

        return func_wrapper

    return check_status_decorator